FROM node:18-alpine AS builder
WORKDIR /app
COPY . .
RUN corepack enable && corepack prepare pnpm@latest-8 --activate \
    && pnpm config set store-dir .pnpm-store \
    && pnpm install --frozen-lockfile \
    && pnpm build


FROM nginx:1.25.3-alpine
COPY --from=builder /app/build /var/www
COPY --from=builder /app/nginx/default.conf /etc/nginx/nginx.conf
ENTRYPOINT ["nginx", "-g", "daemon off;"]
