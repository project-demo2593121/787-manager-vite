import { ConfigEnv, defineConfig, UserConfigExport } from 'vite';
import react from '@vitejs/plugin-react-swc';
import tsconfigPaths from 'vite-tsconfig-paths';
import svgr from 'vite-plugin-svgr';
import dotenv from 'dotenv';
import pluginRewriteAll from 'vite-plugin-rewrite-all';

dotenv.config();

// https://vitejs.dev/config/
// eslint-disable-next-line no-empty-pattern
export default function getConfig({}: ConfigEnv): UserConfigExport {
  return defineConfig({
    plugins: [
      react(),
      tsconfigPaths(),
      svgr(), // Put the Sentry vite plugin after all other plugins
      pluginRewriteAll(),
    ],
    build: {
      assetsDir: '.',
      sourcemap: true,
    },
    server: {
      port: Number(process.env.VITE_APP_PORT) || 4000,
    },
  });
}
