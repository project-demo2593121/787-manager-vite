class Config {
  mode: string;
  appName: string;
  apiUrl: string;

  constructor() {
    this.mode = import.meta.env.MODE || 'development';
    this.appName = import.meta.env.VITE_APP_NAME || '787 Manager';
    this.apiUrl = import.meta.env.VITE_APP_API_URL;
  }
}
export default new Config();
