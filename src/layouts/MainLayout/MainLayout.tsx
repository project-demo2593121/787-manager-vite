import React, {
  FC,
  PropsWithChildren,
  Suspense,
  useCallback,
  useMemo,
  useState,
} from 'react';
import type { MenuProps } from 'antd';
import { Button, Dropdown, Menu } from 'antd';
import { getPath } from 'routes/router-paths';
import {
  AccountBalanceWalletOutlined,
  AccountCircleOutlined,
  AirplaneTicketOutlined,
  CardMembershipOutlined,
  ContentPasteOutlined,
  EmailOutlined,
  FileCopyOutlined,
  FolderCopyOutlined,
  GroupOutlined,
  LocalActivityOutlined,
  LocalAtmOutlined,
  LogoutOutlined,
  ManageSearchOutlined,
  MenuOutlined,
  MoneyOutlined,
  PaidOutlined,
  PaymentOutlined,
  PaymentsOutlined,
  ReceiptLongOutlined,
  SettingsOutlined,
  SupervisedUserCircleOutlined,
} from '@mui/icons-material';
import { observer } from 'mobx-react-lite';
import { useStore } from 'store';
import type { MenuInfo } from 'rc-menu/lib/interface';
import style from './MainLayout.module.scss';
import clsx from 'clsx';
import SplashScreen from 'components/SplashScreen';
import { useNavigate } from 'react-router-dom';

const MainLayout: FC<PropsWithChildren> = ({ children }) => {
  const [selectedMenuKeys, setSelectedMenuKeys] = useState<string[]>(['']);
  const navigate = useNavigate();
  // const location = useLocation();
  const { commonStore, authenticationStore } = useStore();
  const { isSidebarCollapsed, toggleSidebarCollapse } = commonStore;
  const { currentUser } = authenticationStore;

  const userMenuItems = useMemo(
    (): MenuProps['items'] => [
      {
        label: 'Cài đặt',
        icon: <SettingsOutlined />,
        key: getPath('appSetting'),
      },
      {
        type: 'divider',
      },
      {
        label: 'Đăng xuất',
        icon: <LogoutOutlined />,
        key: 'logout',
        danger: true,
      },
    ],
    []
  );

  const onUserMenuClick: MenuProps['onClick'] = useCallback(
    ({ key }: { key: string }) => {
      if (key === 'logout') {
        localStorage.clear();
        navigate(getPath('login'));
        return;
      }
      navigate(key);
    },
    [navigate]
  );

  const items = useMemo(
    (): MenuProps['items'] => [
      {
        key: 'Quản trị nội dung',
        label: 'Nội dung Website',
        type: 'group',
        children: [
          {
            key: getPath('blogCategories'),
            label: 'Danh mục',
            icon: <FolderCopyOutlined />,
          },
          {
            key: getPath('blogPosts'),
            label: 'Bài viết',
            icon: <FileCopyOutlined />,
          },
        ],
      },
      {
        key: 'Chăm sóc khách hàng',
        label: 'Chăm sóc khách hàng',
        type: 'group',
        children: [
          {
            key: 'Quản lý đặt chỗ',
            label: 'Quản lý đặt chỗ',
            icon: <ContentPasteOutlined />,
          },
          {
            key: 'Xuất vé',
            label: 'Xuất vé',
            icon: <AirplaneTicketOutlined />,
          },
        ],
      },
      {
        key: 'Sales',
        label: 'Sales',
        type: 'group',
        children: [
          {
            key: 'Quản lý nhóm',
            label: 'Quản lý nhóm',
            icon: <GroupOutlined />,
          },
          {
            key: 'Quản lý thành viên',
            label: 'Quản lý thành viên',
            icon: <SupervisedUserCircleOutlined />,
          },
          {
            key: 'Quản lý coupon',
            label: 'Quản lý coupon',
            icon: <LocalActivityOutlined />,
          },
          {
            key: 'Gửi email',
            label: 'Gửi email',
            icon: <EmailOutlined />,
          },
        ],
      },
      {
        key: 'Báo cáo',
        label: 'Báo cáo',
        type: 'group',
        children: [
          {
            key: 'Danh sách đặt chỗ',
            label: 'Danh sách đặt chỗ',
            icon: <ReceiptLongOutlined />,
          },
          {
            key: 'Báo cáo cổng thanh toán',
            label: 'Báo cáo cổng thanh toán',
            icon: <PaymentOutlined />,
          },
          {
            key: 'Báo cáo hạn mức nhân viên',
            label: 'Báo cáo hạn mức nhân viên',
            icon: <LocalAtmOutlined />,
          },
          {
            key: 'Lịch sử xuất vé',
            label: 'Lịch sử xuất vé',
            icon: <ManageSearchOutlined />,
          },
        ],
      },
      {
        key: 'Quản trị',
        label: 'Quản trị',
        type: 'group',
        children: [
          {
            key: 'Quản lý tài khoản nhân viên',
            label: 'Quản lý tài khoản nhân viên',
            icon: <PaymentsOutlined />,
          },
          {
            key: 'Quản lý hạn mức nhân viên',
            label: 'Quản lý hạn mức nhân viên',
            icon: <AccountBalanceWalletOutlined />,
          },
          {
            key: 'Quản lý phí dịch vụ',
            label: 'Quản lý phí dịch vụ',
            icon: <PaidOutlined />,
          },
          {
            key: 'Quản lý tính điểm',
            label: 'Quản lý tính điểm',
            icon: <MoneyOutlined />,
          },
          {
            key: 'Quản lý đổi điểm',
            label: 'Quản lý đổi điểm',
            icon: <CardMembershipOutlined />,
          },
        ],
      },
    ],
    []
  );

  const handleClickMenuItem = useCallback(
    (info: MenuInfo) => {
      const { key: navigationPath } = info;
      setSelectedMenuKeys([navigationPath]);
      navigate(navigationPath);
    },
    [navigate]
  );

  // const matchedEditBlogUrl = useRouteMatch(
  //   getPath('editBlogPost', ':postId')
  // )?.url;

  // useEffect(() => {
  //   const activePath = location.pathname;
  //   if ([getPath('addBlogPost'), matchedEditBlogUrl].includes(activePath)) {
  //     setSelectedMenuKeys([getPath('blogPosts')]);
  //     return;
  //   }
  //   setSelectedMenuKeys([activePath]);
  // }, [matchedEditBlogUrl, location.pathname]);

  return (
    <div className={style.mainLayoutWrapper}>
      <aside
        className={clsx(
          style.mainSidebarWrapper,
          isSidebarCollapsed && style.collapsed
        )}
      >
        <div className={style.topSidebarContainer}>
          <figure className={style.logoContainer}>
            <img src="/assets/imgs/Logo.png" alt="" />
          </figure>
          <Dropdown
            menu={{ items: userMenuItems, onClick: onUserMenuClick }}
            placement="bottomRight"
            overlayClassName="user-menu-dropdown"
            trigger={['click']}
          >
            <div className={style.userAvatarWrapper}>
              <AccountCircleOutlined />
            </div>
          </Dropdown>
        </div>
        <div className={style.userInfoWrapper}>
          <div className={style.img}>
            <img src="/assets/imgs/tt_avatar_small.jpg" alt="" />
          </div>
          <div className={style.caption}>
            <p>{currentUser?.displayName ?? ''}</p>
            <p>{currentUser?.email ?? ''}</p>
          </div>
        </div>
        <Menu
          mode="inline"
          items={items}
          theme="dark"
          onClick={handleClickMenuItem}
          selectedKeys={selectedMenuKeys}
        />
      </aside>

      <main
        className={clsx(
          style.mainPageContentWrapper,
          isSidebarCollapsed && style.collapsed
        )}
      >
        <header className={style.fixedHeader}>
          <Button
            type="text"
            onClick={() => toggleSidebarCollapse(!isSidebarCollapsed)}
          >
            <MenuOutlined />
          </Button>
        </header>
        <section className={style.pageContentWrapper}>
          <Suspense fallback={<SplashScreen />}>{children}</Suspense>
        </section>
      </main>
    </div>
  );
};

export default observer(MainLayout);
