import React, { FC, memo, PropsWithChildren, ReactNode } from 'react';
import style from './AuthLayout.module.scss';

type Props = PropsWithChildren & {
  title: string;
  subTitle: ReactNode;
};

const AuthLayout: FC<Props> = ({ children, title, subTitle }) => {
  return (
    <div className={style.authLayoutWrapper}>
      <div className={style.authFormWrapper}>
        <div className={style.formContent}>
          <figure className={style.logoContainer}>
            <img src="/assets/imgs/Logo.png" alt="" />
          </figure>
          <h1 className={style.formHeading}>{title}</h1>
          <div className={style.formSubHeading}>{subTitle}</div>
          {children}
        </div>
      </div>
      <section className={style.welcomeBanner}>
        <img src="/assets/svgs/login.svg" alt="" width={400} height={400} />
      </section>
    </div>
  );
};

export default memo(AuthLayout);
