import { lazy } from 'react';
import {
  createBrowserRouter,
  createRoutesFromElements,
  Route,
  RouterProvider,
  Routes,
} from 'react-router-dom';
import { getPath } from 'routes/router-paths';
import { TAppRoute } from 'routes/types';
import NotFoundPage from 'pages/NotFoundPage';
import MainLayout from 'layouts/MainLayout';

const LoginPage = lazy(() => import('pages/LoginPage'));
const RegisterPage = lazy(() => import('pages/RegisterPage'));
const DashboardPage = lazy(() => import('pages/DashboardPage'));
const SettingsPage = lazy(() => import('pages/SettingsPage'));
const BlogCategoriesPage = lazy(() => import('pages/BlogCategoriesPage'));
const BlogPostsPage = lazy(() => import('pages/BlogPostsPage'));
const AddEditBlogPostPage = lazy(
  () => import('pages/AddEditBlogPostPage/AddEditBlogPostPage')
);

const publicRoutes: TAppRoute[] = [
  {
    path: getPath('login'),
    element: <LoginPage />,
  },
  {
    path: getPath('register'),
    element: <RegisterPage />,
  },
];

const protectedRoutes: TAppRoute[] = [
  {
    path: getPath('dashboard'),
    element: <DashboardPage />,
  },
  {
    path: getPath('home'),
    element: <DashboardPage />,
  },
  {
    path: getPath('appSetting'),
    element: <SettingsPage />,
  },
  {
    path: getPath('blogCategories'),
    element: <BlogCategoriesPage />,
  },
  {
    path: getPath('blogPosts'),
    element: <BlogPostsPage />,
  },
  {
    path: getPath('addBlogPost') || getPath('editBlogPost', ':postId'),
    element: <AddEditBlogPostPage />,
  },
];

const ProtectedRoutes = () => {
  return (
    <Routes>
      {protectedRoutes.map(route => {
        const { path, element } = route;

        return (
          <Route
            key={path}
            path={path}
            element={<MainLayout>{element}</MainLayout>}
          />
        );
      })}

      <Route path="*" element={<NotFoundPage />} />
    </Routes>
  );
};

const router = createBrowserRouter(
  createRoutesFromElements(
    <>
      {publicRoutes.map(route => {
        const { path, element } = route;

        return <Route key={path} path={path} element={element} />;
      })}

      <Route path="/*" element={<ProtectedRoutes />} />
    </>
  )
);

const AppRoutes = () => {
  return <RouterProvider router={router} />;
};

export default AppRoutes;
