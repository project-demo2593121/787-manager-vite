const publicPathConfig = {
  login: () => '/auth/login',
  register: () => '/auth/register',
};

const privatePathConfig = {
  home: () => '/',
  dashboard: () => '/dashboard',
  appSetting: () => '/settings',
  blogCategories: () => '/blog/categories',
  blogPosts: () => '/blog/posts',
  addBlogPost: () => '/blog/posts/add-new',
  editBlogPost: (dataKey: string) => `/blog/posts/edit/${dataKey}`,
};

const pathsMap = {
  ...publicPathConfig,
  ...privatePathConfig,
};

export type PathsMap = typeof pathsMap;

export const getPath = <TRoute extends keyof PathsMap>(
  route: TRoute,
  ...params: Parameters<PathsMap[TRoute]>
) => {
  /* eslint-disable @typescript-eslint/no-explicit-any */
  const pathCb: (...args: any[]) => string = pathsMap[route];
  return pathCb(...params);
};
