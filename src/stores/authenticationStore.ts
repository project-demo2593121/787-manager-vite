import { makeAutoObservable } from 'mobx';
import { TUserInfo } from 'AppModels';

export class AuthenticationStore {
  constructor() {
    makeAutoObservable(this);
  }

  /** Current user */
  currentUser: TUserInfo = {} as TUserInfo;
  setUserInfo = (userInfo: TUserInfo) => {
    this.currentUser = userInfo;
  };
}

const authenticationStore = new AuthenticationStore();
export default authenticationStore;
