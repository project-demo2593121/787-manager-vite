import commonStore, { CommonStore } from './commonStore';
import authenticationStore, {
  AuthenticationStore,
} from 'stores/authenticationStore';
import blogCategoryStore, { BlogCategoryStore } from 'stores/blogCategoryStore';
import blogPostStore, { BlogPostStore } from 'stores/blogPostStore';

export type RootStore = {
  authenticationStore: AuthenticationStore;
  commonStore: CommonStore;
  blogCategoryStore: BlogCategoryStore;
  blogPostStore: BlogPostStore;
};

const rootStore: RootStore = {
  authenticationStore,
  commonStore,
  blogCategoryStore,
  blogPostStore,
};

export default rootStore;
