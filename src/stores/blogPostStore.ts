import { makeAutoObservable } from 'mobx';
import { TBlogPost } from 'AppModels';

export class BlogPostStore {
  constructor() {
    makeAutoObservable(this);
  }

  /** Post list */
  postList: TBlogPost[] = [];
  setPostList = (postList: TBlogPost[]) => {
    this.postList = postList;
  };

  /** Post count */
  postCount = 0;
  setPostCount = (postCount: number) => {
    this.postCount = postCount;
  };

  /** Post detail */
  postDetail?: TBlogPost;
  setPostDetail = (data: TBlogPost | undefined) => {
    this.postDetail = data;
  };
}

const blogPostStore = new BlogPostStore();
export default blogPostStore;
