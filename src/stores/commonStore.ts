import { makeAutoObservable } from 'mobx';

export class CommonStore {
  constructor() {
    makeAutoObservable(this);
  }

  /** App theme */
  appTheme = localStorage.getItem('appTheme') || '#ab9332';
  setTheme = (themeName: string) => {
    this.appTheme = themeName;
    localStorage.setItem('appTheme', themeName);
  };

  /** Offline mode */
  isOffline = false;
  checkIsOffline = (state: boolean) => {
    this.isOffline = state;
  };

  /** Sidebar collapsed */
  isSidebarCollapsed = false;
  toggleSidebarCollapse = (state: boolean) => {
    this.isSidebarCollapsed = state;
  };
}

const commonStore = new CommonStore();
export default commonStore;
