import { makeAutoObservable } from 'mobx';
import { TBlogCategory } from 'AppModels';

export class BlogCategoryStore {
  constructor() {
    makeAutoObservable(this);
  }

  categories: TBlogCategory[] = [];
  setCategories = (categories: TBlogCategory[]) => {
    this.categories = categories;
  };
}

const blogCategoryStore = new BlogCategoryStore();
export default blogCategoryStore;
