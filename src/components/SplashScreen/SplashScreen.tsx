import React from 'react';
import style from './SplashScreen.module.scss';
import clsx from 'clsx';

type Props = {
  fullscreen?: boolean;
};

const SplashScreen = ({ fullscreen }: Props) => {
  return (
    <div className={clsx(style.wrapper, fullscreen && style.fullscreen)}>
      <div className={style.inner}>
        <span className={style.title}>
          <span className={style.dot}></span>
        </span>
        <span className={style.title}>
          <span className={style.dot}></span>
        </span>
        <span className={style.title}>
          <span className={style.dot}></span>
        </span>
        <span className={style.title}>
          <span className={style.dot}></span>
        </span>
        <span className={style.title}>
          <span className={style.dot}></span>
        </span>
      </div>
    </div>
  );
};

export default SplashScreen;
