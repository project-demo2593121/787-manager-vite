import React, { FC, memo, ReactNode } from 'react';
import style from './PageHeader.module.scss';

type Props = {
  title: string;
  addon?: ReactNode;
};
const PageHeader: FC<Props> = ({ title, addon }) => {
  return (
    <div className={style.pageHeaderWrapper}>
      <h1>{title}</h1>
      {addon}
    </div>
  );
};

export default memo(PageHeader);
