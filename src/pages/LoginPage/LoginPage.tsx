import React, { useEffect } from 'react';
import LoginForm from 'features/auth/components/LoginForm';
import AppHelmet from 'components/Helmet';
import AuthLayout from 'layouts/AuthLayout';
import { useNavigate } from 'react-router-dom';
import { getPath } from 'routes/router-paths';

const LoginPage = () => {
  const navigate = useNavigate();

  useEffect(() => {
    if (localStorage.getItem('jwt')) {
      navigate(getPath('dashboard'));
    }
  }, [navigate]);

  return (
    <AuthLayout
      title="Đăng nhập"
      subTitle={
        <p>
          Chưa có tài khoản?{' '}
          <span
            onClick={() => {
              navigate(getPath('register'));
            }}
          >
            Đăng ký
          </span>
        </p>
      }
    >
      <AppHelmet title="Đăng nhập" />
      <LoginForm />
    </AuthLayout>
  );
};

export default LoginPage;
