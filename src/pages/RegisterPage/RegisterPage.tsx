import React from 'react';
import AuthLayout from 'layouts/AuthLayout';
import { getPath } from 'routes/router-paths';
import AppHelmet from 'components/Helmet';
import RegisterForm from 'features/auth/components/RegisterForm';

const RegisterPage = () => {
  return (
    <AuthLayout
      title="Đăng ký"
      subTitle={
        <p>
          Đã có tài khoản?{' '}
          {/*<span onClick={() => history.push(getPath('login'))}>Đăng nhập</span>*/}
        </p>
      }
    >
      <AppHelmet title="Đăng ký" />
      <RegisterForm />
    </AuthLayout>
  );
};

export default RegisterPage;
