import React, { FC } from 'react';
import AppHelmet from 'components/Helmet';
import { Button, Result } from 'antd';
import { useNavigate } from 'react-router-dom';
import { getPath } from 'routes/router-paths';
import style from './NotFoundPage.module.scss';

const NotFoundPage: FC = () => {
  const navigate = useNavigate();

  return (
    <main className={style.pageWrapper}>
      <AppHelmet title="Không tìm thấy trang" />
      <Result
        icon={
          <img src="/assets/svgs/404.svg" alt="404" width={350} height={350} />
        }
        title={<strong>Không tìm thấy trang</strong>}
        subTitle="Trang bạn tìm kiếm không tồn tại. Vui lòng quay lại trang trước hoặc trở lại trang chủ."
        extra={
          <div className={style.buttonRow}>
            <Button onClick={() => navigate(-1)}>Quay lại trang trước</Button>

            <Button type="primary" onClick={() => navigate(getPath('home'))}>
              Về trang chủ
            </Button>
          </div>
        }
      />
    </main>
  );
};

export default NotFoundPage;
