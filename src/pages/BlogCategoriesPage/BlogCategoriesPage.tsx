import React from 'react';
import AppHelmet from 'components/Helmet';
import BlogCategories from 'features/blog-category/components/BlogCategories';

const BlogCategoriesPage = () => {
  return (
    <>
      <AppHelmet title="Danh mục bài viết" />
      <BlogCategories />
    </>
  );
};

export default BlogCategoriesPage;
