import React from 'react';
import AppHelmet from 'components/Helmet';
import AddEditBlogPost from 'features/blog-post/components/AddEditBlogPost';

const AddEditBlogPostPage = () => {
  return (
    <>
      <AppHelmet title="Thêm bài viết mới" />
      <AddEditBlogPost />
    </>
  );
};

export default AddEditBlogPostPage;
