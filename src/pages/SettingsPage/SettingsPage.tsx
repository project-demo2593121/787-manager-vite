import React, { useState } from 'react';
import style from './SettingsPage.module.scss';
import PageHeader from 'components/PageHeader';
import { AccountCircleOutlined, LockOpen } from '@mui/icons-material';
import clsx from 'clsx';
import AppHelmet from 'components/Helmet';

enum ACTIVE_MENU {
  ACCOUNT = 'account',
  SECURITY = 'security',
}

const SettingsPage = () => {
  const [activeMenu, setActiveMenu] = useState<ACTIVE_MENU>(
    ACTIVE_MENU.ACCOUNT
  );

  return (
    <>
      <AppHelmet title="Cài đặt" />
      <div className={style.pageWrapper}>
        <aside className={style.sidebar}>
          <PageHeader title="Cài đặt" />
          <ul className={style.menuList}>
            <li
              className={clsx(
                activeMenu === ACTIVE_MENU.ACCOUNT && style.active
              )}
              onClick={() => setActiveMenu(ACTIVE_MENU.ACCOUNT)}
            >
              <AccountCircleOutlined />
              <div>
                <h2>Tài khoản</h2>
                <p>Cập nhật thông tin và các dữ liệu cá nhân của tài khoản</p>
              </div>
            </li>
            <li
              className={clsx(
                activeMenu === ACTIVE_MENU.SECURITY && style.active
              )}
              onClick={() => setActiveMenu(ACTIVE_MENU.SECURITY)}
            >
              <LockOpen />
              <div>
                <h2>Đổi mật khẩu</h2>
                <p>Cập nhật mật khẩu đăng nhập của hệ thống 787.vn</p>
              </div>
            </li>
          </ul>
        </aside>
        <main className={style.main}>
          <p>Ahihi</p>
        </main>
      </div>
    </>
  );
};

export default SettingsPage;
