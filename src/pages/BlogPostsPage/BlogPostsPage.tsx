import React from 'react';
import AppHelmet from 'components/Helmet/AppHelmet';
import BlogPosts from 'features/blog-post/components/BlogPosts';

const BlogPostsPage = () => {
  return (
    <>
      <AppHelmet title="Danh sách bài viết" />
      <BlogPosts />
    </>
  );
};

export default BlogPostsPage;
