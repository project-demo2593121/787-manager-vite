import { Suspense, useCallback, useEffect } from 'react';
import { ConfigProvider } from 'antd';
import vi_VN from 'antd/locale/vi_VN';
import ScrollRestoration from 'components/ScrollRestoration';
import SplashScreen from 'components/SplashScreen';
import AppRoutes from 'routes/AppRoutes';
import { useStore } from 'store';
import { getCurrentUser } from '@features/auth/services';

import 'styles/variables.scss';
import 'styles/typography.scss';
import 'styles/global.scss';
import 'antd/dist/reset.css';

import 'styles/variables.scss';
import 'styles/typography.scss';
import 'styles/global.scss';
import 'antd/dist/reset.css';

const App = () => {
  const {
    commonStore: { appTheme, setTheme },
    authenticationStore: { setUserInfo },
  } = useStore();

  const appInitializer = useCallback(async () => {
    const currentTheme = localStorage.getItem('appTheme');
    setTheme(currentTheme || '#ab9332');
    const accessToken = localStorage.getItem('accessToken');
    if (!accessToken) return;
    try {
      const userInfo = await getCurrentUser();
      setUserInfo(userInfo);
    } catch (e) {}
  }, [setTheme, setUserInfo]);

  useEffect(() => {
    void appInitializer();
  }, [appInitializer]);

  return (
    <Suspense
      fallback={
        <ScrollRestoration>
          <SplashScreen fullscreen />
        </ScrollRestoration>
      }
    >
      <ConfigProvider
        locale={vi_VN}
        theme={{
          token: {
            colorPrimary: appTheme,
            borderRadius: 5,
          },
        }}
      >
        <AppRoutes />
      </ConfigProvider>
    </Suspense>
  );
};

export default App;
