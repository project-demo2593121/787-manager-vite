export enum BLOG_POST_STATUS {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE',
}

export enum BLOG_POST_TYPE {
  NORMAL = 'NORMAL',
}
