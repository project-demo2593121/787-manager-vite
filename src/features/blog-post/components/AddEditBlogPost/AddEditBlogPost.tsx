import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import PageHeader from 'components/PageHeader';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import style from './AddEditBlogPost.module.scss';
import { Button, Form, Input, message, Select, Upload } from 'antd';
import { useParams, useNavigate } from 'react-router-dom';
import { getPath } from 'routes/router-paths';
import { StringMap } from 'quill';
import { useStore } from 'store';
import { getAllBlogCategories } from 'features/blog-category/services';
import { observer } from 'mobx-react-lite';
import MoveToInboxOutlinedIcon from '@mui/icons-material/MoveToInboxOutlined';
import { BLOG_POST_STATUS, BLOG_POST_TYPE } from 'features/blog-post/constants';
import { RcFile } from 'antd/es/upload/interface';
import { TBlogPostCreatePayload } from 'AppModels';
import {
  createBlogPost,
  getBlogPostDetail,
  updateBlogPost,
} from 'features/blog-post/services';
import { AxiosError } from 'axios';
import { convertTitleToSlug } from 'utils/string';

const { Dragger } = Upload;
const { Option } = Select;

const AddEditBlogPost = () => {
  const navigate = useNavigate();
  const { postId } = useParams<{ postId: string }>();

  const quillRef = useRef<ReactQuill>(null);

  const [form] = Form.useForm();
  const { setFieldsValue } = form;

  const { blogCategoryStore, blogPostStore } = useStore();
  const { categories, setCategories } = blogCategoryStore;
  const { postDetail, setPostDetail } = blogPostStore;

  const [selectedFile, setSelectedFile] = useState('');
  const [editorContent, setEditorContent] = useState('');
  const [imgLoadFailed, setImgLoadFailed] = useState(false);

  const fetchBlogCategories = useCallback(async () => {
    try {
      const response = await getAllBlogCategories();
      setCategories(response);
    } catch (e) {}
  }, [setCategories]);

  const getBlogPostDetailData = useCallback(async () => {
    if (!postId) return;
    try {
      const response = await getBlogPostDetail(postId);
      setPostDetail(response);
    } catch (e) {
      const statusCode = (e as AxiosError)?.response?.status;
      if (statusCode === 404) {
        // TODO: BẮn sang 404 page
      }
    }
  }, [postId, setPostDetail]);

  useEffect(() => {
    if (!postDetail) return;
    setFieldsValue(postDetail);
    setEditorContent(postDetail?.body ?? '');
    setSelectedFile(postDetail?.thumb ?? '');
  }, [postDetail, setFieldsValue]);

  useEffect(() => {
    void fetchBlogCategories();
    void getBlogPostDetailData();
  }, [fetchBlogCategories, getBlogPostDetailData]);

  useEffect(() => () => setPostDetail(undefined), [setPostDetail]);

  const toggleHtmlMode = useCallback(() => {
    const editor = quillRef?.current?.getEditor();
    if (editor) {
      console.log(quillRef?.current?.makeUnprivilegedEditor(editor).getHTML());
    }
  }, []);

  const customToolbarModule = useMemo(
    (): StringMap => ({
      toolbar: {
        container: [
          [{ font: [] }],
          [{ header: [1, 2, 3, 4, 5, 6, false] }],
          ['bold', 'italic', 'underline', 'strike'],
          [{ align: [] }],
          [{ list: 'ordered' }, { list: 'bullet' }],
          [{ color: [] }, { background: [] }, 'link', 'image'],
          ['clean'],
          ['html'],
        ],
        handlers: {
          html: toggleHtmlMode,
        },
      },
      clipboard: { matchVisual: true },
      keyboard: {
        bindings: {
          indent: {
            key: 'Tab',
            handler(_: Range, data: { offset: number }) {
              return data.offset !== 0;
            },
          },
          outdent: {
            key: 'Tab',
            shiftKey: true,
            handler() {
              return false;
            },
          },
        },
      },
    }),
    [toggleHtmlMode]
  );

  const onFinish = useCallback(
    async (values: TBlogPostCreatePayload) => {
      const payload = {
        ...values,
        body: editorContent,
        type: BLOG_POST_TYPE.NORMAL,
        status: BLOG_POST_STATUS.ACTIVE,
        thumb: selectedFile,
      };
      try {
        const response = postId
          ? await updateBlogPost({ ...payload, dataKey: postId })
          : await createBlogPost(payload);
        if (!postId) {
          message.success('Tạo bài viết thành công');
          navigate(getPath('blogPosts'));
          return;
        }
        message.success('Cập nhật bài viết thành công');
        setPostDetail(response);
      } catch (e) {}
    },
    [editorContent, navigate, postId, selectedFile, setPostDetail]
  );

  const onFileSelected = useCallback((file: RcFile) => {
    const fileReader = new FileReader();
    fileReader.addEventListener('load', () => {
      const base64String = fileReader.result as string;
      setSelectedFile(base64String);
    });
    fileReader.readAsDataURL(file);
    return false;
  }, []);

  const onRemoveFile = useCallback(() => {
    setSelectedFile('');
  }, []);

  const renderedEditor = useMemo(
    () => (
      <ReactQuill
        ref={quillRef}
        modules={customToolbarModule}
        theme="snow"
        value={editorContent}
        onChange={setEditorContent}
        preserveWhitespace
      />
    ),
    [customToolbarModule, editorContent]
  );

  const postTitle = Form.useWatch('title', form);

  useEffect(() => {
    if (!postTitle) {
      form.setFieldsValue({
        url: '',
      });
      return;
    }
    form.setFieldsValue({
      url: convertTitleToSlug(postTitle),
    });
  }, [postTitle, form]);

  return (
    <>
      <PageHeader title={postDetail ? 'Sửa bài viết' : 'Thêm bài viết mới'} />
      <Form form={form} layout="vertical" onFinish={onFinish}>
        <div className={style.formRow}>
          <div className={style.formCol}>
            <Form.Item
              name="title"
              label="Tên bài"
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập tên bài viết',
                },
              ]}
            >
              <Input placeholder="Nhập tên bài viết" />
            </Form.Item>
            <Form.Item
              name="url"
              label="URL"
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập URL cho bài viết',
                },
                {
                  pattern: /^[a-zA-Z0-9-]+$/,
                  message:
                    'Custom URL chỉ cho phép nhập chữ cái không dấu, số và gạch nối',
                },
              ]}
            >
              <Input placeholder="Nhập URL cho bài viết" />
            </Form.Item>
            <Form.Item
              name="description"
              label="Mô tả"
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập mô tả cho bài viết',
                },
              ]}
            >
              <Input.TextArea placeholder="Mô tả" rows={3} />
            </Form.Item>
          </div>
          <div className={style.formCol}>
            <Form.Item
              name="categoryDataKey"
              label="Danh mục bài viết"
              rules={[
                {
                  required: true,
                  message: 'Vui lòng chọn danh mục bài viết',
                },
              ]}
            >
              <Select
                allowClear
                showSearch
                placeholder="Chọn danh mục bài viết"
                optionLabelProp="label"
                optionFilterProp="label"
              >
                {categories.map(item => (
                  <Option
                    key={item.dataKey}
                    value={item.dataKey}
                    label={item.name}
                  >
                    {item.name}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item label="Ảnh đại diện">
              {postDetail?.thumb && !imgLoadFailed && (
                <div className={style.thumbnailContainer}>
                  <img
                    src={postDetail.thumb}
                    alt=""
                    onError={() => setImgLoadFailed(true)}
                  />
                </div>
              )}
              <Dragger beforeUpload={onFileSelected} onRemove={onRemoveFile}>
                <MoveToInboxOutlinedIcon className={style.uploadIcon} />
                <p className={style.uploadText}>
                  Click hoặc kéo thả để upload file
                </p>
                {postDetail?.thumb && !imgLoadFailed && (
                  <p className={style.uploadCaption}>
                    Ảnh mới upload sẽ được thay thế cho ảnh cũ
                  </p>
                )}
              </Dragger>
            </Form.Item>
          </div>
        </div>
        {renderedEditor}
        <div className={style.buttonRow}>
          <Button type="primary" htmlType="submit">
            {postDetail ? 'Lưu thay đổi' : 'Tạo bài viết'}
          </Button>
          <Button type="text" onClick={() => navigate(getPath('blogPosts'))}>
            Hủy
          </Button>
        </div>
      </Form>
    </>
  );
};

export default observer(AddEditBlogPost);
