import styled from 'styled-components';
import { Button, Form } from 'antd';

export const StyledButton = styled(Button)`
  display: flex;
  align-items: center;
  column-gap: 5px;

  svg {
    font-size: 16px;
  }
`;

export const StyledFormItem = styled(Form.Item)`
  flex: 1;
  margin-bottom: 0;

  &:last-child {
    flex: 0;
  }
`;
