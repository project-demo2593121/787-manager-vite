import React, {
  Dispatch,
  FC,
  SetStateAction,
  useCallback,
  useEffect,
} from 'react';
import { TGetBlogPostQueryParams } from 'AppModels';
import style from './BlogPostSearchFilterBar.module.scss';
import { Button, Form, Select } from 'antd';
import { observer } from 'mobx-react-lite';
import { useStore } from 'store';
import { getAllBlogCategories } from 'features/blog-category/services';
import { StyledFormItem } from 'features/blog-post/components/BlogPosts/BlogPosts.styled';

type Props = {
  setQueryParams: Dispatch<SetStateAction<Partial<TGetBlogPostQueryParams>>>;
};

const { Option } = Select;

const BlogPostSearchFilterBar: FC<Props> = ({ setQueryParams }) => {
  const [form] = Form.useForm();
  const { blogCategoryStore } = useStore();
  const { categories, setCategories } = blogCategoryStore;

  const fetchBlogCategories = useCallback(async () => {
    try {
      const response = await getAllBlogCategories();
      setCategories(response);
    } catch (e) {}
  }, [setCategories]);

  const onFinish = useCallback(
    (values: Partial<TGetBlogPostQueryParams>) => {
      setQueryParams(values);
    },
    [setQueryParams]
  );

  useEffect(() => {
    void fetchBlogCategories();
  }, [fetchBlogCategories]);

  return (
    <section className={style.wrapper}>
      <Form form={form} layout="vertical" onFinish={onFinish}>
        <div className={style.formRow}>
          <StyledFormItem name="categoryDataKey" label="Danh mục">
            <Select
              allowClear
              showSearch
              placeholder="Chọn danh mục bài viết"
              optionLabelProp="label"
              optionFilterProp="label"
            >
              {categories.map(item => (
                <Option
                  key={item.dataKey}
                  value={item.dataKey}
                  label={item.name}
                >
                  {item.name}
                </Option>
              ))}
            </Select>
          </StyledFormItem>
          <StyledFormItem name="status" label="Trạng thái">
            <Select placeholder="Lọc theo trạng thái" allowClear>
              <Option value="ACTIVE">ACTIVE</Option>
              <Option value="INACTIVE">INACTIVE</Option>
            </Select>
          </StyledFormItem>
          <StyledFormItem>
            <Button type="primary" htmlType="submit">
              Lọc kết quả
            </Button>
          </StyledFormItem>
        </div>
      </Form>
    </section>
  );
};

export default observer(BlogPostSearchFilterBar);
