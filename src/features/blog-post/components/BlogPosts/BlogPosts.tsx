import React, { useCallback, useEffect, useState } from 'react';
import PageHeader from 'components/PageHeader';
import { ColumnsType } from 'antd/es/table';
import {
  Dropdown,
  Input,
  message,
  Modal,
  Switch,
  Table,
  Typography,
} from 'antd';
import { useNavigate } from 'react-router-dom';
import { getPath } from 'routes/router-paths';
import MoreVertOutlinedIcon from '@mui/icons-material/MoreVertOutlined';
import { TBlogPost, TGetBlogPostQueryParams } from 'AppModels';
import { deleteBlogPost, getAllBlogPost } from 'features/blog-post/services';
import { observer } from 'mobx-react-lite';
import { useStore } from 'store';
import dayjs from 'dayjs';
import { BLOG_POST_STATUS } from 'features/blog-post/constants';
import ErrorIcon from '@mui/icons-material/Error';
import style from './BlogPosts.module.scss';
import TuneIcon from '@mui/icons-material/Tune';
import { StyledButton } from 'features/blog-post/components/BlogPosts/BlogPosts.styled';
import NoteAddIcon from '@mui/icons-material/NoteAdd';
import BlogPostSearchFilterBar from 'features/blog-post/components/BlogPosts/BlogPostSearchFilterBar';

const { Paragraph } = Typography;
const { Search } = Input;
const { confirm } = Modal;
const paragraphConfig = { rows: 1, expandable: false };

const BlogPosts = () => {
  const navigate = useNavigate();
  const { blogPostStore } = useStore();
  const { setPostList, postList, postCount, setPostCount } = blogPostStore;
  const [pageIndex, setPageIndex] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [searchKeyword, setSearchKeyword] = useState<string>();
  const [showFilter, setShowFilter] = useState(false);
  const [queryParams, setQueryParams] = useState(
    {} as Partial<TGetBlogPostQueryParams>
  );

  const fetchAllBlogPost = useCallback(
    async (payload: TGetBlogPostQueryParams) => {
      const searchPayload = {
        ...payload,
      };
      try {
        const { totalItems, items } = await getAllBlogPost(searchPayload);
        setPostList(items);
        setPostCount(totalItems);
      } catch (e) {}
    },
    [setPostCount, setPostList]
  );

  const handleDeletePost = useCallback(async (item: TBlogPost) => {
    const { dataKey } = item;
    try {
      await deleteBlogPost(dataKey);
      message.success('Bài viết đã được xóa');
    } catch (e) {}
  }, []);

  const renderDropdownItems = useCallback(
    (item: TBlogPost) => {
      return [
        {
          key: '1',
          label: 'Chỉnh sửa',
          onClick: () => {
            navigate(getPath('editBlogPost', item.dataKey));
          },
        },
        {
          key: '2',
          label: 'Xóa',
          danger: true,
          onClick: () => {
            confirm({
              title: (
                <div className="ant-modal-confirm-heading">
                  <ErrorIcon className="icon-danger" />
                  <span>Bạn có chắc chắn muốn xóa bài viết này?</span>
                </div>
              ),
              icon: null,
              content: 'Bài viết đã xóa sẽ không thể hoàn tác được',
              onOk() {
                void handleDeletePost(item);
              },
              okType: 'danger',
            });
          },
        },
      ];
    },
    [handleDeletePost, navigate]
  );

  const columns: ColumnsType<TBlogPost> = [
    {
      title: 'Tiêu đề',
      key: 'title',
      render: (record: TBlogPost) => (
        <Paragraph ellipsis={paragraphConfig} title={record.title}>
          {record.title}
        </Paragraph>
      ),
    },
    {
      title: 'Danh mục',
      key: 'category',
      width: 200,
      dataIndex: 'category',
    },
    {
      title: 'Mô tả',
      key: 'description',
      render: (record: TBlogPost) => (
        <Paragraph ellipsis={paragraphConfig} title={record.description}>
          {record.description}
        </Paragraph>
      ),
    },
    {
      title: 'Ngày tạo',
      key: 'createDate',
      width: 200,
      render: (record: TBlogPost) =>
        dayjs(record.createDate).format('DD/MM/YYYY HH:mm'),
    },
    {
      title: 'Trạng thái',
      key: 'status',
      width: 100,
      align: 'center',
      render: (record: TBlogPost) => (
        <Switch checked={Boolean(record.status === BLOG_POST_STATUS.ACTIVE)} />
      ),
    },
    {
      title: '',
      key: 'action',
      align: 'right',
      fixed: 'right',
      width: 80,
      render: (record: TBlogPost) => {
        return (
          <Dropdown
            menu={{ items: renderDropdownItems(record) }}
            placement="bottomRight"
            trigger={['click']}
          >
            <div className="actionMenuIcon">
              <MoreVertOutlinedIcon />
            </div>
          </Dropdown>
        );
      },
    },
  ];

  useEffect(() => {
    void fetchAllBlogPost({
      pageSize,
      pageIndex,
      ...(searchKeyword && { keyword: searchKeyword }),
      ...queryParams,
    });
  }, [fetchAllBlogPost, pageIndex, pageSize, searchKeyword, queryParams]);

  return (
    <>
      <PageHeader
        title="Bài viết"
        addon={
          <div className={style.buttonRow}>
            <Search
              enterButton={false}
              placeholder="Nhập từ khóa tìm kiếm"
              onSearch={value => setSearchKeyword(value)}
              allowClear
            />
            <StyledButton onClick={() => setShowFilter(!showFilter)}>
              <TuneIcon />
              Lọc
            </StyledButton>
            <StyledButton
              type="primary"
              onClick={() => navigate(getPath('addBlogPost'))}
            >
              <NoteAddIcon />
              Thêm bài viết
            </StyledButton>
          </div>
        }
      />
      <div className="tablePageWrapper">
        {showFilter && (
          <BlogPostSearchFilterBar setQueryParams={setQueryParams} />
        )}
        <Table
          rowKey={record => record.dataKey}
          columns={columns}
          dataSource={postList ?? []}
          pagination={{
            total: postCount,
            current: pageIndex,
            pageSize,
            showSizeChanger: true,
            onChange: (p, ps) => {
              setPageIndex(p);
              setPageSize(ps);
            },
          }}
          scroll={{ x: 1000 }}
        />
      </div>
    </>
  );
};

export default observer(BlogPosts);
