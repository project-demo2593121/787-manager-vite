declare module 'AppModels' {
  import { BLOG_POST_STATUS, BLOG_POST_TYPE } from '@/features/blog-post/constants';

  export type TBlogPostResponse = {
    totalItems: number;
    items: TBlogPost[];
  };

  export type TBlogPostCreatePayload = {
    categoryDataKey: string;
    title: string;
    description: string;
    url: string;
    thumb?: string;
    body?: string;
    type?: BLOG_POST_TYPE;
    status?: BLOG_POST_STATUS;
  };

  export type TBlogPostUpdatePayload = TBlogPostCreatePayload & {
    dataKey: string;
  };

  export type TBlogPost = TBlogPostCreatePayload & {
    dataKey: string;
    createDate: string;
    category: string;
  };

  export type TGetBlogPostQueryParams = {
    keyword?: string;
    categoryDataKey?: string;
    status?: BLOG_POST_STATUS;
    pageIndex: number;
    pageSize: number;
  };
}
