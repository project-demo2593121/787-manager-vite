import {
  TBlogPost,
  TBlogPostCreatePayload,
  TBlogPostResponse,
  TBlogPostUpdatePayload,
  TGetBlogPostQueryParams,
} from 'AppModels';
import httpClient from 'utils/httpClient';

const endpoint = '/PostData';

export const getAllBlogPost = (params: TGetBlogPostQueryParams) => {
  return httpClient<TBlogPostResponse>({
    url: `${endpoint}/Search`,
    method: 'get',
    params,
  });
};

export const createBlogPost = (payload: TBlogPostCreatePayload) => {
  return httpClient<TBlogPost>({
    url: `${endpoint}/Insert`,
    method: 'post',
    data: payload,
  });
};

export const deleteBlogPost = (postId: string) => {
  return httpClient({
    url: `${endpoint}/Delete`,
    method: 'delete',
    data: { dataKey: postId },
  });
};

export const updateBlogPost = (payload: TBlogPostUpdatePayload) => {
  return httpClient<TBlogPost>({
    url: `${endpoint}/Update`,
    method: 'patch',
    data: payload,
  });
};

export const getBlogPostDetail = (dataKey: string) => {
  return httpClient<TBlogPost>({
    url: `${endpoint}/Get`,
    method: 'get',
    params: { dataKey },
  });
};
