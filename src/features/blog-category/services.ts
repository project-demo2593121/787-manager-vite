import {
  TBlogCategory,
  TBlogCategoryCreatePayload,
  TBlogCategoryUpdatePayload,
} from 'AppModels';
import httpClient from 'utils/httpClient';

const endpoint = '/CategoryData';

export const getAllBlogCategories = () => {
  return httpClient<TBlogCategory[]>({
    url: `${endpoint}/Search`,
    method: 'get',
  });
};

export const createBlogCategory = (payload: TBlogCategoryCreatePayload) => {
  return httpClient<TBlogCategory[]>({
    url: `${endpoint}/Insert`,
    method: 'post',
    data: payload,
  });
};

export const deleteBlogCategory = (categoryId: string) => {
  return httpClient({
    url: `${endpoint}/Delete`,
    method: 'delete',
    params: { dataKey: categoryId },
  });
};

export const updateBlogCategory = (payload: TBlogCategoryUpdatePayload) => {
  return httpClient<TBlogCategory[]>({
    url: `${endpoint}/Update`,
    method: 'patch',
    data: payload,
  });
};
