import React, { useCallback, useEffect, useState } from 'react';
import {
  deleteBlogCategory,
  getAllBlogCategories,
} from 'features/blog-category/services';
import PageHeader from 'components/PageHeader';
import {
  Button,
  Dropdown,
  message,
  Modal,
  Switch,
  Table,
  Tag,
  Typography,
} from 'antd';
import type { ColumnsType } from 'antd/es/table';
import AddEditBlogCategoryModal from 'features/blog-category/components/AddEditBlogCategoryModal';
import { useStore } from 'store';
import { observer } from 'mobx-react-lite';
import { TBlogCategory } from 'AppModels';
import { BLOG_CATEGORY_STATUS } from 'features/blog-category/constants';
import dayjs from 'dayjs';
import MoreVertOutlinedIcon from '@mui/icons-material/MoreVertOutlined';
import ErrorIcon from '@mui/icons-material/Error';

const { Paragraph } = Typography;
const { confirm } = Modal;
const paragraphConfig = { rows: 1, expandable: false };

const BlogCategories = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const { blogCategoryStore } = useStore();
  const { categories, setCategories } = blogCategoryStore;
  const [pageIndex, setPageIndex] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [selectedItem, setSelectedItem] = useState<TBlogCategory>();

  const fetchBlogCategories = useCallback(async () => {
    try {
      const response = await getAllBlogCategories();
      setCategories(response);
      setPageIndex(1);
    } catch (e) {}
  }, [setCategories]);

  const handleDeleteCategory = useCallback(
    async (item: TBlogCategory) => {
      const { dataKey } = item;
      try {
        await deleteBlogCategory(dataKey);
        await fetchBlogCategories();
        message.success('Danh mục đã được xóa');
      } catch (e) {}
    },
    [fetchBlogCategories]
  );

  const renderDropdownItems = useCallback(
    (item: TBlogCategory) => {
      return [
        {
          key: '1',
          label: 'Chỉnh sửa',
          onClick: () => {
            setSelectedItem(item);
            setIsModalOpen(true);
          },
        },
        {
          key: '2',
          label: 'Xóa',
          danger: true,
          onClick: () => {
            confirm({
              title: (
                <div className="ant-modal-confirm-heading">
                  <ErrorIcon className="icon-danger" />
                  <span>Bạn có chắc chắn muốn xóa danh mục này?</span>
                </div>
              ),
              icon: null,
              content: 'Danh mục đã xóa sẽ không thể hoàn tác được',
              onOk() {
                void handleDeleteCategory(item);
              },
              okType: 'danger',
            });
          },
        },
      ];
    },
    [handleDeleteCategory]
  );

  const columns: ColumnsType<TBlogCategory> = [
    {
      title: 'Tên danh mục',
      key: 'name',
      width: 150,
      render: (record: TBlogCategory) => (
        <Paragraph ellipsis={paragraphConfig} title={record.name}>
          {record.name}
        </Paragraph>
      ),
    },
    {
      title: 'URL',
      key: 'url',
      width: 60,
      render: (record: TBlogCategory) => <Tag>{record.url}</Tag>,
    },
    {
      title: 'Mô tả',
      key: 'description',
      width: 150,
      render: (record: TBlogCategory) => (
        <Paragraph ellipsis={paragraphConfig} title={record.description}>
          {record.description}
        </Paragraph>
      ),
    },
    {
      title: 'Ngày tạo',
      key: 'createDate',
      width: 200,
      render: (record: TBlogCategory) =>
        dayjs(record.createDate).format('DD/MM/YYYY HH:mm'),
    },
    {
      title: 'Trạng thái',
      key: 'status',
      width: 100,
      align: 'center',
      render: (record: TBlogCategory) => (
        <Switch
          checked={Boolean(record.status === BLOG_CATEGORY_STATUS.ACTIVE)}
        />
      ),
    },
    {
      title: '',
      key: 'action',
      align: 'right',
      fixed: 'right',
      width: 80,
      render: (record: TBlogCategory) => {
        return (
          <Dropdown
            menu={{ items: renderDropdownItems(record) }}
            placement="bottomRight"
            trigger={['click']}
          >
            <div className="actionMenuIcon">
              <MoreVertOutlinedIcon />
            </div>
          </Dropdown>
        );
      },
    },
  ];

  useEffect(() => {
    void fetchBlogCategories();
  }, [fetchBlogCategories]);

  return (
    <>
      <PageHeader
        title="Danh mục"
        addon={
          <Button type="primary" onClick={() => setIsModalOpen(true)}>
            Thêm danh mục
          </Button>
        }
      />

      <div className="tablePageWrapper">
        <Table
          rowKey={record => record.dataKey}
          columns={columns}
          dataSource={categories}
          pagination={{
            current: pageIndex,
            pageSize,
            total: categories.length,
            showSizeChanger: true,
            onChange: (p, ps) => {
              setPageIndex(p);
              setPageSize(ps);
            },
          }}
        />
      </div>

      <AddEditBlogCategoryModal
        open={isModalOpen}
        setOpen={setIsModalOpen}
        selectedItem={selectedItem}
      />
    </>
  );
};

export default observer(BlogCategories);
