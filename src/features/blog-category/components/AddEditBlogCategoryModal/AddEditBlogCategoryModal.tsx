import React, {
  Dispatch,
  FC,
  SetStateAction,
  useCallback,
  useEffect,
  useState,
} from 'react';
import { Button, Form, Input, message, Modal, Upload } from 'antd';
import style from 'features/blog-category/components/AddEditBlogCategoryModal/AddEditBlogCategoryModal.module.scss';
import type { TBlogCategory, TBlogCategoryCreatePayload } from 'AppModels';
import { TBlogCategoryUpdatePayload } from 'AppModels';
import {
  createBlogCategory,
  getAllBlogCategories,
  updateBlogCategory,
} from 'features/blog-category/services';
import MoveToInboxOutlinedIcon from '@mui/icons-material/MoveToInboxOutlined';
import { RcFile } from 'antd/es/upload/interface';
import { useStore } from 'store';
import { observer } from 'mobx-react-lite';
import { convertTitleToSlug } from 'utils/string';

type Props = {
  open: boolean;
  setOpen: Dispatch<SetStateAction<boolean>>;
  selectedItem?: TBlogCategory;
};

const { Dragger } = Upload;

const AddEditBlogCategoryModal: FC<Props> = ({
  open,
  setOpen,
  selectedItem,
}) => {
  const { blogCategoryStore } = useStore();
  const { setCategories } = blogCategoryStore;

  const [form] = Form.useForm();
  const { setFieldsValue } = form;

  const [selectedFile, setSelectedFile] = useState<string>();

  useEffect(() => {
    setFieldsValue(selectedItem);
    setSelectedFile(selectedItem?.thumb ?? undefined);
  }, [selectedItem, setFieldsValue]);

  const handleClose = useCallback(() => {
    setOpen(false);
    form.resetFields();
  }, [form, setOpen]);

  const onSubmit = useCallback(
    async (values: TBlogCategoryCreatePayload) => {
      const payload = {
        ...values,
        ...(selectedFile && { thumb: selectedFile }),
        ...(selectedItem && { dataKey: selectedItem.dataKey }),
      };
      try {
        selectedItem
          ? await updateBlogCategory(payload as TBlogCategoryUpdatePayload)
          : await createBlogCategory(payload);
        const categories = await getAllBlogCategories();
        setCategories(categories);
        message.success(
          selectedItem
            ? 'Lưu thay đổi thành công'
            : 'Tạo mới danh mục thành công'
        );
        handleClose();
      } catch (e) {}
    },
    [handleClose, selectedFile, selectedItem, setCategories]
  );

  const onFileSelected = useCallback((file: RcFile) => {
    const fileReader = new FileReader();
    fileReader.addEventListener('load', () => {
      const base64String = fileReader.result as string;
      setSelectedFile(base64String);
    });
    fileReader.readAsDataURL(file);
    return false;
  }, []);

  const onRemoveFile = useCallback(() => {
    setSelectedFile(undefined);
  }, []);

  const categoryName = Form.useWatch('name', form);

  useEffect(() => {
    if (!categoryName) {
      form.setFieldsValue({
        url: '',
      });
      return;
    }
    form.setFieldsValue({
      url: convertTitleToSlug(categoryName),
    });
  }, [categoryName, form]);

  return (
    <Modal
      open={open}
      title={selectedItem ? 'Sửa danh mục' : 'Thêm danh mục'}
      footer={null}
      onCancel={handleClose}
    >
      <Form form={form} layout="vertical" onFinish={onSubmit}>
        <Form.Item
          name="name"
          label="Tên danh mục"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập tên danh mục',
            },
          ]}
        >
          <Input placeholder="Nhập tên danh mục" />
        </Form.Item>
        <Form.Item
          name="url"
          label="URL"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập url',
            },
            {
              pattern: /^[a-zA-Z0-9-]+$/,
              message:
                'Custom URL chỉ cho phép nhập chữ cái không dấu, số và gạch nối',
            },
          ]}
        >
          <Input placeholder="Nhập custom URL" />
        </Form.Item>
        <Form.Item name="body" label="Body">
          <Input.TextArea placeholder="Nhập nội dung danh mục" rows={3} />
        </Form.Item>
        <Form.Item name="description" label="Mô tả">
          <Input.TextArea placeholder="Nhập mô tả danh mục" rows={3} />
        </Form.Item>
        <Form.Item label="Ảnh đại diện">
          <Dragger beforeUpload={onFileSelected} onRemove={onRemoveFile}>
            <MoveToInboxOutlinedIcon className={style.uploadIcon} />
            <p className={style.uploadText}>
              Click hoặc kéo thả để upload file
            </p>
          </Dragger>
        </Form.Item>
        <div className={style.buttonRow}>
          <Button onClick={handleClose} type="text">
            Hủy
          </Button>
          <Button type="primary" htmlType="submit">
            {selectedItem ? 'Lưu thay đổi' : 'Tạo danh mục'}
          </Button>
        </div>
      </Form>
    </Modal>
  );
};

export default observer(AddEditBlogCategoryModal);
