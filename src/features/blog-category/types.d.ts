declare module 'AppModels' {
  import { BLOG_CATEGORY_STATUS } from '@/features/blog-category/constants';

  export type TBlogCategoryCreatePayload = {
    name: string;
    url: string;
    description?: string;
    thumb?: string;
    body?: string;
    status?: BLOG_CATEGORY_STATUS;
  };

  export type TBlogCategoryUpdatePayload = TBlogCategoryCreatePayload & {
    dataKey: string;
  };

  export type TBlogCategory = {
    dataKey: string;
    name: string;
    url: string;
    description: string;
    body: string;
    thumb: string;
    createDate: string;
    status: BLOG_CATEGORY_STATUS;
  };
}
