import type { TAccountLoginPayload } from 'AppModels';
import { TAccountLoginResponse, TUserInfo } from 'AppModels';
import httpClient from 'utils/httpClient';

const endpoint = '/UserAuth';

export const userLogin = (payload: TAccountLoginPayload) => {
  return httpClient<TAccountLoginResponse>({
    url: `${endpoint}/Authenticate`,
    method: 'post',
    data: payload,
  });
};

export const getCurrentUser = () => {
  return httpClient<TUserInfo>({
    url: `${endpoint}/Me`,
    method: 'get',
  });
};
