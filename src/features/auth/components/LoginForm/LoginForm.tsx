import React, { useCallback, useState } from 'react';
import { Button, Checkbox, Form, Input, message } from 'antd';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { useStore } from 'store';
import { TAccountLoginPayload } from 'AppModels';
import { userLogin } from 'features/auth/services';
import { getPath } from 'routes/router-paths';
import { observer } from 'mobx-react-lite';
import style from './LoginForm.module.scss';

const LoginForm = () => {
  const navigate = useNavigate();
  const { commonStore, authenticationStore } = useStore();
  const { appTheme } = commonStore;
  const { setUserInfo } = authenticationStore;
  const [form] = Form.useForm();
  const location = useLocation();
  const [isRememberLogin, setIsRememberLogin] = useState(true);

  const handleLogin = useCallback(
    async (payload: TAccountLoginPayload) => {
      try {
        const response = await userLogin(payload);
        if (!response) return;
        const { accessToken, agentUser } = response;
        setUserInfo(agentUser);
        message.success(`Xin chào, ${agentUser.displayName}`);
        isRememberLogin
          ? localStorage.setItem('accessToken', accessToken)
          : sessionStorage.setItem('accessToken', accessToken);
        const previousLocation = location?.state?.from?.pathname;
        navigate(previousLocation ?? getPath('dashboard'));
      } catch (e) {}
    },
    [isRememberLogin, location?.state?.from?.pathname, navigate, setUserInfo]
  );

  return (
    <Form form={form} layout="vertical" onFinish={handleLogin}>
      <Form.Item
        name="userName"
        label="Tên đăng nhập"
        rules={[{ required: true, message: 'Vui lòng nhập tên đăng nhập!' }]}
      >
        <Input placeholder="Tên đăng nhập" />
      </Form.Item>

      <Form.Item
        name="password"
        label="Mật khẩu"
        rules={[{ required: true, message: 'Vui lòng nhập mật khẩu!' }]}
      >
        <Input.Password placeholder="Mật khẩu" />
      </Form.Item>

      <div className={style.formRow}>
        <Checkbox
          checked={isRememberLogin}
          onChange={e => setIsRememberLogin(e.target.checked)}
        >
          Ghi nhớ đăng nhập
        </Checkbox>
        <Link to="" style={{ color: appTheme }}>
          Quên mật khẩu
        </Link>
      </div>

      <Form.Item>
        <Button block type="primary" htmlType="submit">
          Đăng nhập
        </Button>
      </Form.Item>
    </Form>
  );
};

export default observer(LoginForm);
