declare module 'AppModels' {
  export type TAccountLoginPayload = {
    username: string;
    password: string;
  };

  export type TAccountLoginResponse = {
    accessToken: string;
    expireInSeconds: number;
    agentUser: TUserInfo;
  };

  export type TUserInfo = {
    displayName: string;
    userName: string;
    email: string;
    phone: string;
    role: string;
  };
}
