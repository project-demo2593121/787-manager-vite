import { AxiosHeaders, RawAxiosRequestHeaders } from 'axios';

declare module 'AppModels' {
  export type HttpClientProps = {
    url: string;
    method: 'get' | 'post' | 'put' | 'patch' | 'delete';
    customHeader?: (RawAxiosRequestHeaders & MethodsHeaders) | AxiosHeaders;
    params?: Record<string, any>;
    data?: Record<string, any>;
  };
}
