import axios, { AxiosError, AxiosRequestConfig } from 'axios';
import config from 'config';
import { HttpClientProps } from 'AppModels';

const baseURL = config.apiUrl;

const httpClient = <ReturnType>(config: HttpClientProps) => {
  const { url, method, customHeader, params, data } = config;
  const accessToken = localStorage.accessToken || sessionStorage.accessToken;

  const axiosConfig: AxiosRequestConfig = {
    baseURL,
    url,
    method,
    ...(customHeader ?? {
      headers: {
        ...(accessToken && { Authorization: `Bearer ${accessToken}` }),
      },
    }),
    ...(params && { params }),
    ...(data && { data }),
  };

  return new Promise<ReturnType>((resolve, reject) =>
    axios(axiosConfig)
      .then(response => resolve(response.data.data))
      .catch((e: AxiosError) => reject(e))
  );
};

export default httpClient;
